/**
 * @file
 *   ofhlinw.module: Finds links in forms and makes them open in a new window.
 */

Drupal.behaviors.ofhlinw = function() {
  var selector = '#' + Drupal.settings.ofhlinwFormId + ' a[@href]';
  // Should we warn users that they will leave the form and lose
  // changes if they click other links on the page (but not in the
  // form)?
  $(selector).each(function() {
    // Do a few further testss on the link before changing it's behaviour.
    var e = $(this);
    var href = e.attr('href');
    var http = 'http';
    if (href && (href.substring(0, 1) == '/' || href.substring(0, http.length) == http)) {
      // Add the icon.
      e.addClass('new-window-link');
      e.click(function() {
        // Open in a new window if the link is clicked.
        window.open(this.href);
        // And don't allow the event to bubble!
        return false;
      });
    }
  });
};
